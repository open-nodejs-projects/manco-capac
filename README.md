[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
![NPM version](https://img.shields.io/badge/npm-5.0.3-green.svg)
![Node Js version](https://img.shields.io/badge/node-8.1.2-green.svg)


**WORK IN PROGRESS**
<br/>
# Manco Capac is an scaffold for nodejs projects based on es6

we recomend use [it snippet](https://gitlab.com/open-nodejs-projects/manco-capac/snippets/1668972) to download and install dependencies. <br/>
You are free to edit the project name and install other dependencies using NPM or YARN. <br/>
Also, the project is designed to support [standard js](https://standardjs.com) and babel es6 compile.

## How it´s work ?

before start any nodejs app, the project run validations like [standard js](https://standardjs.com) to validate standard coding and [nsp](https://nodesecurity.io) to validate vulnerabilities in your dependencies. 

## What about node version?

The nodejs version used here is located in the [nvm file](./.nvmrc)

